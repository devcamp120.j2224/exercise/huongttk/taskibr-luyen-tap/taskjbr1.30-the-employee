import com.devcamp.jbr130.Employee;

public class App {
    public static void main(String[] args) {
        Employee employee1 = new Employee(1, "Cao", "Tuan", 1000);
        Employee employee2 = new Employee(2, "Truong", "Huong", 2000);

        // In luong
        System.out.println("Luong 1 nam");
        System.out.println(employee1);
        System.out.println(employee2);
        // In sau tang luong
        System.out.println("Sau khi tang luong: ");
        employee1.raiseSalary(10);
        employee2.raiseSalary(20);
        System.out.println(employee1);
        System.out.println(employee2);
    }
}
