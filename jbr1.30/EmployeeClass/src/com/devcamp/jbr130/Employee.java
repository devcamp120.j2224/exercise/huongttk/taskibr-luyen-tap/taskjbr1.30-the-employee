package com.devcamp.jbr130;

public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private int salary;

    public Employee(int id, String firstName, String lastName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }

    /**
     * @return int
     */
    public int getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return String
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return String
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return int
     */
    public int getSalary() {
        return salary;
    }

    /**
     * @param salary
     */
    public void setSalary(int salary) {
        this.salary = salary;
    }

    /**
     * @return int
     */
    public int getAnnualSalary() {
        return this.salary * 12;
    }

    /**
     * @param percent
     * @return int
     */
    public int raiseSalary(int percent) {
        int raiseSalary = this.salary + (this.salary*percent/100);
        this.setSalary(raiseSalary);
        return raiseSalary;
    }

    /**
     * @return String
     */
    @Override
    public String toString() {
        return "Employee: " + firstName + " " + lastName + ", salary: " + salary
                + ", Luong 1 nam la: " + this.getAnnualSalary();
    }
}
